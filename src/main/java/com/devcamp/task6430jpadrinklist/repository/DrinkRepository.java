package com.devcamp.task6430jpadrinklist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6430jpadrinklist.model.CDrink;

public interface DrinkRepository extends JpaRepository<CDrink, Long>{
    CDrink findById(long id);
}
