package com.devcamp.task6430jpadrinklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task6430JpaDrinkListApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task6430JpaDrinkListApplication.class, args);
	}

}
